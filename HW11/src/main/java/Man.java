public final class Man extends Human{
    private Family family;

    public Man(String name, String surname, String birthdate) {
        super(name, surname, birthdate);
    }

    public Man(String name, String surname, String birthdate, int iq) {
        super(name, surname, birthdate, iq);
    }

    public Man(String name, String surname, String birthdate, Family family, int iq, Pet pet) {
        super(name, surname, birthdate, family, iq, pet);
    }
    public void greetPet(){
        System.out.printf("Привет %s, я твой хозяин \n", this.family.getPets());
    }
    public void setFamily(Family family){
        this.family = family;
    }
    public void repairCar(){
        System.out.println("нужно опять фиксить машину %*!$^# !");
    }
}

