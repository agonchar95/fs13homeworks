import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Human {
    private String name;
    private String surname;
    private long birthdate = new Date().getTime();
    private int iq;
    private Pet pet;
    private Family family;
    private final Map<DayOfWeek, String> schedule = new HashMap<>();

    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    public Human(String name, String surname, String birthdate) {
        this.name = name;
        this.surname = surname;
        this.birthdate = setYear(birthdate);
    }

    public Human(String name, String surname, String birthdate, int iq) {
        this(name, surname, birthdate);
        this.iq = iq;
    }

    public Human(String name, String surname, String birthdate, Family family) {
        this(name, surname, birthdate);
        this.family = family;
    }

    public Human(String name, String surname, String birthdate, Family family, int iq, Pet pet) {
        this(name, surname, birthdate, family);
        this.iq = iq;
        this.pet = pet;
    }

    public Human() {

    }

    public String getName() {
        return this.name;
    }

    public String getSurname() {
        return this.surname;
    }

    public long getYear() {
        return this.birthdate;
    }

    public int getIq() {
        return this.iq;
    }

    public Pet getPet() {
        return this.pet;
    }

    public Map<DayOfWeek, String> getSchedule() {
        return this.schedule;
    }

    public Family getFamily() {
        return this.family;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long setYear(String birthdate) {
        Date date = null;
        try {
             date = sdf.parse(birthdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long result = date.getTime();
        return this.birthdate = result;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void setSchedule(DayOfWeek day, String task) {
        this.schedule.put(day, task);
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return this.getClass() + "{ " +
                "name='" + this.name + '\'' + ", surname='" +
                this.surname + '\'' + ",birthdate = " + sdf.format(this.birthdate) + ",family = " + this.family +
                ", iq = " + this.iq + ",schedule = "
                + this.schedule + ", " + this.pet + "}";
    }

    protected void finalize() {
        System.out.printf("%s was deleted right now", this.toString());
    }

    void greetPet() {
        System.out.printf("Привет, %s\n", pet.getNickname());
    }

    void describePet() {
        String trick = (pet.getTrickLevel() <= 50 ? "почти не хитрый" : "очень хитрый");
        System.out.printf("У меня есть %s, eмy %d лет, он %s\n", pet.getSpecies(), pet.getAge(), trick);
    }

    public String describeAge() {
        long now = new Date().getTime();
        long birthdate = this.birthdate;
        int years = new Date(now).getYear() - new Date(birthdate).getYear();
        int months = new Date(now).getMonth() - new Date(birthdate).getMonth();
        if (months < 0) {
            months = 12 + months;
            years = years - 1;
        }
        int days = new Date(now).getDate() - new Date(birthdate).getDate();
        if (days < 0) {
            days = 30 + days;
        }
        return years + " years " + months + " months " + days + " days";
    }

    public String prettyFormat(){
        return String.format("%s: name: '%s', surname: '%s', date of birth: '%s', iq: '%d', schedule: '%s'", this.getClass().getSimpleName(), this.name, this.surname, sdf.format(this.birthdate), this.iq, this.schedule);
    }

}
