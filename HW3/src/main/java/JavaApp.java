import java.util.Scanner;

public class JavaApp {
    public static String[][] createSchedule() {
        String[][] schedule = new String[7][2];
        String[] days = {"sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"};
        String[] tasks = {"sleep", "read", "work", "cry", "pray", "dream", "chill"};
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 2; j++) {
                schedule[i][j] = j == 0 ? days[i] : tasks[i];
            }
        }
        return schedule;
    }

    public static void result(String[][] schedule, int day) {
        System.out.printf("Your tasks for %s: %s\n", schedule[day][0], schedule[day][1]);
    }

    public static void main(String[] args) {
        String[][] schedule = createSchedule();
        Scanner scanner = new Scanner(System.in);
        boolean stop = false;
        while (!stop) {
            System.out.println("Please, input the day of the week: ");
            String selectedDay = scanner.next().toLowerCase();
            switch (selectedDay) {
                case "sunday" -> result(schedule, 0);
                case ("monday") -> result(schedule, 1);
                case ("tuesday") -> result(schedule, 2);
                case ("wednesday") -> result(schedule, 3);
                case ("thursday") -> result(schedule, 4);
                case ("friday") -> result(schedule, 5);
                case ("saturday") -> result(schedule, 6);
                case ("exit") -> stop = true;
                default -> System.out.println("Sorry, I don't understand you, please try again.");
            }
        }
    }
}
