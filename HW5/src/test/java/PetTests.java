import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PetTests {
    Pet Igor = new Pet(Species.Horse, "Igor");

    @Test
    void petToStringTest() {
        String expexted = "Horse{nickname = 'Igor', age = 0, trickLevel = 0, habits = null}";
        assertEquals(expexted, Igor.toString());
    }
}
