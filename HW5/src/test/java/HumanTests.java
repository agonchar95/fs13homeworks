import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class HumanTests {
    Human Vasya = new Human("Vasya", "Ivanov", 1998);

    @Test
    void humanToStringTest() {
        String expected = "class Human{ name='Vasya', surname='Ivanov',year = 1998,family = null, iq = 0,schedule = [[null, null]], null}";
        assertEquals(expected, Vasya.toString());
    }
}
