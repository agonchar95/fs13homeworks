public class Main {
    public static void main(String[] args) {
        String[] tasks = {
                "Work",
                "Pray",
                "Lay",
                "Fly",
                "Cry",
                "Chill",
                "Dream"};

        Human Vasya = new Human("Vasya", "Ivanov", 1995);
        Human Masha = new Human("Masha", "Ivanova", 1997);
        Human Dasha = new Human("Dasha", "Ivanova", 2020);
//        for (int i = 0; i < DayOfWeek.values().length; i++) {
//            Dasha.setSchedule(DayOfWeek.values()[i], tasks[i]);
//        }
        Family family1 = new Family(Vasya, Masha);
        family1.addChild(Dasha);
        System.out.println(family1);
        System.out.println("---------------------------------------------------");

        Pet Chih = new Pet(Species.Dog, "Chih");
        System.out.println(Chih);
        Dasha.setPet(Chih);
        System.out.println(Dasha);
        Dasha.greetPet();
        Dasha.describePet();
        System.out.println("---------------------------------------------------");

//        for (int i = 0; i < 12000; i++) {
//            Human Kolya = new Human("Kolya", "Petrov", 1995);
//            Human Sveta = new Human("Sveta", "Sidorova", 1985);
//        }

    }
}
