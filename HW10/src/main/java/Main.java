
public class Main {
    public static void main(String[] args) {
        FamilyController familyController = new FamilyController();
        Man Vasya = new Man("Vasya", "Pechkin", "14/4/1995");
        Woman Masya = new Woman("Masya", "Pechkina", "13/5/1997");
        Man Kolya = new Man("Kolya", "Ivanov", "1/4/1993");
        Woman Olya = new Woman("Olya", "Ivanova", "11/11/1999");
        Man Andrej = new Man("Andrej", "Kozlov", "29/8/1992");

        Family family1 = familyController.createNewFamily(Masya, Vasya);
        Family family2 = familyController.createNewFamily(Olya, Kolya);
        familyController.bornChild(family1, "Misha", "Masha");
        familyController.adoptChild(family2,Andrej);
        familyController.count();
        System.out.println("--------------------------------------");
        familyController.displayAllFamilies();
        System.out.println("---------------------------------------");
        System.out.println("Vasya - " + Vasya);
        System.out.println("Masya - " + Masya);
        System.out.println("Kolya - " + Kolya);
        System.out.println("Olya - " + Olya);
    }
}
