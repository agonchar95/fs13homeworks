import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {
    final List<Family> familyData = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
        return this.familyData;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return this.familyData.get(index);
    }

    @Override
    public Family deleteFamily(int index) {
        return this.familyData.remove(index);
    }

    @Override
    public Boolean deleteFamily(Family family) {
        return this.familyData.remove(family);
    }

    @Override
    public Family saveFamily(Family family) {
        this.familyData.add(family);
        return family;
    }
}
