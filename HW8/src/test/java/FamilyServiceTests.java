import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


public class FamilyServiceTests {
    static FamilyService familyService;

    @Test
    public void getAllFamilies() {
        FamilyService familyService = new FamilyService();
        List<Family> families = new ArrayList<>();
        Woman w1 = new Woman("female", "asffsa", 1986);
        Man m1 = new Man("male", "hsahsa", 1990);
        Family f1 = new Family(w1, m1);
        families.add(f1);
        familyService.createNewFamily(w1, m1);
        Assert.assertFalse(familyService.getAllFamilies() == families);
    }

    @Test
    public void count() {
        FamilyService familyService = new FamilyService();
        Woman w1 = new Woman("female", "asffsa", 1986);
        Man m1 = new Man("male", "hsahsa", 1990);
        familyService.createNewFamily(w1, m1);
        Assert.assertTrue(familyService.count() == 1);
    }

    @Test
    public void countFamiliesWithMemberNumber() {
        FamilyService familyService = new FamilyService();
        Woman w1 = new Woman("female", "asffsa", 1986);
        Man m1 = new Man("male", "hsahsa", 1990);
        familyService.createNewFamily(w1, m1);
        Woman w2 = new Woman("female", "sdffds", 1986);
        Man m2 = new Man("male", "sfdfds", 1990);
        familyService.createNewFamily(w2, m2);
        int result = familyService.countFamiliesWithMemberNumber(2).size();
        Assert.assertTrue(result == 2);
    }
}
