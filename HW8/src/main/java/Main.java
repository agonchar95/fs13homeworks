public class Main {
    public static void main(String[] args) {
        FamilyController familyController = new FamilyController();
        Man Vasya = new Man("Vasya", "Pechkin", 1994);
        Woman Masya = new Woman("Masya", "Pechkina", 1991);
        Man Kolya = new Man("Kolya", "Ivanov", 1993);
        Woman Olya = new Woman("Olya", "Ivanova", 1997);
        Man Andrej = new Man("Andrej", "Kozlov", 2010);

        Family family1 = familyController.createNewFamily(Masya, Vasya);
        Family family2 = familyController.createNewFamily(Olya, Kolya);
        familyController.bornChild(family1, "Misha", "Masha");
        familyController.adoptChild(family2,Andrej);
        familyController.count();
        System.out.println("--------------------------------------");
        familyController.displayAllFamilies();
    }
}
