import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class FamilyService {
    public FamilyDao familyDao;

    public FamilyService() {
        familyDao = new CollectionFamilyDao();
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        List<Family> all = familyDao.getAllFamilies();
        System.out.printf("These are all families: %s \n", all);
    }

    public List<Family> getFamiliesBiggerThan(int number) {
        List<Family> all = familyDao.getAllFamilies();
        List<Family> matched = new ArrayList<>();
        all.forEach(family -> {
            int ppl = family.familySize();
            if (ppl > number) {
                matched.add(family);
            }
        });
        System.out.println(matched);
        return matched;
    }

    public List<Family> getFamiliesLessThan(int number) {
        List<Family> all = familyDao.getAllFamilies();
        List<Family> matched = new ArrayList<>();
        all.forEach(family -> {
            int ppl = family.familySize();
            if (ppl < number) {
                matched.add(family);
            }
        });
        System.out.println(matched);
        return matched;
    }

    public List<Family> countFamiliesWithMemberNumber(int number) {
        List<Family> all = familyDao.getAllFamilies();
        List<Family> matched = new ArrayList<>();
        all.forEach(family -> {
            int ppl = family.familySize();
            if (ppl == number) {
                matched.add(family);
            }
        });
        System.out.println(matched);
        return matched;
    }

    public Family createNewFamily(Woman woman, Man man) {
        return familyDao.saveFamily(new Family(woman, man));
    }

    public Family deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }

    public Family bornChild(Family family, String maleName, String femaleName) {
        int rand = (int) (Math.random() * 2);

        Human child = rand == 0 ?
                new Human(maleName, family.familyName(), LocalDate.now().getYear())
                :
                new Human(femaleName, family.familyName(), LocalDate.now().getYear());
        family.addChild(child);
        return family;
    }

    public Family adoptChild(Family family, Human child) {
        List<Family> all = familyDao.getAllFamilies();
        int index = all.indexOf(family);
        Family selected = all.get(index);
        selected.addChild(child);
        return selected;
    }

    public void deleteAllChildrenOlderThen(int age) {
        List<Family> all = familyDao.getAllFamilies();
        all.forEach(family -> {
            List<Human> allChildren = family.getChildren();
            allChildren.forEach(child -> {
                int childAge = LocalDate.now().getYear() - child.getYear();
                if (childAge > age) family.deleteChild(child);
            });
        });
    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return familyDao.getFamilyByIndex(index);
    }

    public ArrayList<Pet> getPets(int index) {
        Family family = familyDao.getFamilyByIndex(index);
        return family.getPets();
    }

    public ArrayList<Pet> addPet(int index, Pet pet) {
        Family family = familyDao.getFamilyByIndex(index);
        family.setPets(pet);
        return family.getPets();
    }
}
