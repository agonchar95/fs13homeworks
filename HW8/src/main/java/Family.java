import java.util.ArrayList;


public class Family {
    private Human mother;
    private Human father;
    private final ArrayList<Human> children = new ArrayList<>();
    private final ArrayList<Pet> pets = new ArrayList<>();

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
    }

    public Family(Human mother, Human father, Human child) {
        this.mother = mother;
        this.father = father;
        this.addChild(child);
    }

    public Human getMother() {
        return this.mother;
    }

    public Human getFather() {
        return this.father;
    }

    public ArrayList<Human> getChildren() {
        return this.children;
    }

    public ArrayList<Pet> getPets() {
        return this.pets;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public void setChildren(Human child) {
        this.addChild(child);
    }

    public void setPets(Pet pet) {
        this.pets.add(pet);
    }

    @Override
    public String toString() {
        return this.mother.toString() + "\n" +
                this.father.toString() + "\n" + "children " +
                this.children + "\n";
    }

    protected void finalize() {
        System.out.printf("%s was deleted right now", this.toString());
    }

    public void addChild(Human child) {
        this.children.add(child);
    }

    public void deleteChild(Human child) {
        this.children.remove(child);
    }

    public int findChild(Human child){
        return this.children.indexOf(child);
    }

    public int childrenVolume () {
        return this.children.size();
    }

    public int familySize () {return childrenVolume() + 2;}

    public String familyName () {
        return this.father.getSurname();
    }


}
