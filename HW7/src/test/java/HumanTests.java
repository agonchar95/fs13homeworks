import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class HumanTests {
    Human Vasya = new Human("Vasya", "Ivanov", 1998);

    @Test
    public void humanToStringTest() {
        String expected = "class Human{ name='Vasya', surname='Ivanov',year = 1998,family = null, iq = 0,schedule = {}, null}";
        assertEquals(expected, Vasya.toString());
    }
}
