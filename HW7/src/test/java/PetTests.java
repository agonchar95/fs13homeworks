import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class PetTests {
    RoboCat Igor = new RoboCat( "Igor");

    @Test
    public void petToStringTest() {
        String expected = "{nickname = 'Igor', age = 0, trickLevel = 0, habits = []}";
        assertEquals(expected, Igor.toString());
    }
}
