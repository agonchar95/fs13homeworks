import org.junit.Test;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class FamilyTests {
    @Test
    public void addChild() {
        Human mother = new Human("Vasya", "Ivanov", 1995);
        Human father = new Human("Masha", "Ivanova", 1995);
        Family family = new Family(mother, father);
        family.addChild(new Human("Kolya", "Ivanov", 2015));
        String expected = "[class Human{ name='Kolya', surname='Ivanov',year = 2015,family = null, iq = 0,schedule = {}, null}]";
        assertEquals(expected, family.getChildren().toString());
    }

    @Test
    public void deleteChildSuccess() {
        Human mother = new Human("Vasya", "Ivanov", 1995);
        Human father = new Human("Masha", "Ivanova", 1995);
        Human child = new Human("Kolya", "Ivanov", 2015);
        Family family = new Family(mother, father, child);
        family.deleteChild(child);
        int expected = 0;
        assertEquals(expected, family.childrenVolume());
    }

    @Test
    public void deleteChildFail() {
        Human mother = new Human("Vasya", "Ivanov", 1995);
        Human father = new Human("Masha", "Ivanova", 1995);
        Human child = new Human("Kolya", "Ivanov", 2015);
        Human child2 = new Human("Ivan", "Ivanov", 2016);
        Family family = new Family(mother, father, child);
        int index = family.findChild(child2);
        boolean isInFamily = index >= 0;
        assertFalse(isInFamily);
    }

    @Test
    public void testToString() {
        Human mother = new Human("Vasya", "Ivanov", 1995);
        Human father = new Human("Masha", "Ivanova", 1997);
        Human child = new Human("Dasha", "Ivanova", 2020);
        Family family = new Family(mother, father, child);
        String expected = "class Human{ name='Vasya', surname='Ivanov',year = 1995,family = null, iq = 0,schedule = {}, null}\n" +
                "class Human{ name='Masha', surname='Ivanova',year = 1997,family = null, iq = 0,schedule = {}, null}\n" +
                "children [class Human{ name='Dasha', surname='Ivanova',year = 2020,family = null, iq = 0,schedule = {}, null}]\n";
        assertEquals(expected, family.toString());
    }
}
