public final class Man extends Human{
    private Family family;

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, Family family, int iq, Pet pet) {
        super(name, surname, year, family, iq, pet);
    }
    public void greetPet(){
        System.out.printf("Привет %s, я твой хозяин \n", this.family.getPet());
    }
    public void setFamily(Family family){
        this.family = family;
    }
    public void repairCar(){
        System.out.println("нужно опять фиксить машину %*!$^# !");
    }
}

