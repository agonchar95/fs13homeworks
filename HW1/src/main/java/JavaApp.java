import java.util.Random;
import java.util.Scanner;

public class JavaApp {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Type your name: ");
        String name = input.nextLine();
        System.out.println("Let the game begin!");
        Random random = new Random();

        do {
            int x = random.nextInt(101);
            System.out.println("Type a number: ");
            int y = input.nextInt();
            if (y == x) {
                System.out.printf("Congratulations, %s \n", name);
            } else if (y > x) {
                System.out.println("Your number is too big. Please, try again.");
            } else {
                System.out.println("Your number is too small. Please, try again.");
            }
        } while (true);
    }
}
