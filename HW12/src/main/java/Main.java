import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class Main {
    public static FamilyController familyController;

    static {
        try {
            familyController = new FamilyController();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static Scanner sc = new Scanner(System.in);

    public static void printMenu() {
        System.out.print(
                "- 1. Заполнить тестовыми данными (автоматом создать несколько семей и сохранить их в базе)\n" +
                        "- 2. Отобразить весь список семей из базы\n" +
                        "- 3. Отобразить список семей, где количество людей больше заданного\n" +
                        "- 4. Отобразить список семей, где количество людей меньше заданного\n" +
                        "- 5. Подсчитать количество семей, где количество членов равно\n" +
                        "- 6. Создать новую семью\n" +
                        "- 7. Удалить семью по индексу семьи в общем списке\n" +
                        "- 8. Редактировать семью по индексу семьи в общем списке \n" +
                        "- 9. Удалить всех детей старше возраста (во всех семьях удаляются дети старше указанного возраста - будем считать, что они выросли)\n");
    }

    public static void createFamily() {
        System.out.println("Введите имя матери: ");
        String motherName = sc.nextLine();
        System.out.println("Введите фамилию матери: ");
        String motherSurname = sc.nextLine();
        System.out.println("Введите дату рождения матери: ");
        String motherBDate = sc.nextLine();
        System.out.println("Введите iq матери: ");
        int motherIq = sc.nextInt();
        if (motherName == null || motherSurname == null || motherBDate == null) throw new IllegalArgumentException();
        Woman mother = new Woman(motherName, motherSurname, motherBDate, motherIq);
        System.out.println("Введите имя отца: ");
        String fatherName = sc.nextLine();
        System.out.println("Введите фамилию отца: ");
        String fatherSurname = sc.nextLine();
        System.out.print("Введите дату рождения отца: ");
        String fatherBDate = sc.nextLine();
        System.out.println("Введите iq отца: ");
        int fatherIq = sc.nextInt();
        if (fatherName == null || fatherSurname == null || fatherBDate == null) throw new IllegalArgumentException();
        Man father = new Man(fatherName, fatherSurname, fatherBDate, fatherIq);
        familyController.createNewFamily(mother, father);
    }

    public static void getFamMoreThan(){
        System.out.println("Введите количество:");
        int num = sc.nextInt();
        if (num >5) throw new FamilyOverflowException("Wrong number");
        familyController.getFamiliesBiggerThan(num).forEach(System.out::println);
    }
    public static void getFamLessThan(){
        System.out.println("Введите количество:");
        int num = sc.nextInt();
        if (num >5) throw new FamilyOverflowException("Wrong number");
        familyController.getFamiliesLessThan(num).forEach(System.out::println);
    }
    public static void getFamAs(){
        System.out.println("Введите количество:");
        int num = sc.nextInt();
        if (num >5) throw new FamilyOverflowException("Wrong number");
        int i = familyController.countFamiliesWithMemberNumber(num);
        System.out.printf("Всего таких семей: %d \n", i);
    }
    public static void rmFamilyByID(){
        System.out.println("Введите порядковый номер:");
        int index = sc.nextInt();
        if (index < 0 || index > 5) throw new FamilyOverflowException("Wrong number");
        familyController.deleteFamilyByIndex(index - 1);
    }

    public static void bornChild() {
        System.out.println("Введите порядковый номер семьи: ");
        int index = sc.nextInt();
        if (index < 0 || index > 5) throw new FamilyOverflowException("Wrong number");
        Family family = familyController.getFamilyById(index);
        System.out.println("Какое имя дать мальчику? ");
        String maleName = sc.nextLine();
        System.out.println("А девочке? ");
        String femaleName = sc.nextLine();
        if (maleName == null || femaleName == null) throw new IllegalArgumentException();
        familyController.bornChild(family, maleName, femaleName);
    }

    public static void adoptChild() {
        System.out.println("Введите номер семьи: ");
        int index = sc.nextInt();
        if (index < 0 || index > 5) throw new FamilyOverflowException("Wrong number");
        Family family = familyController.getFamilyById(index - 1);
        System.out.println("Введите имя ребенка: ");
        String name = sc.nextLine();
        System.out.println("Введите фамилию ребенка: ");
        String surname = sc.nextLine();
        System.out.println("Введите дату рождения ребенка: ");
        String birthdate = sc.nextLine();
        System.out.println("Введите iq ребенка: ");
        int iq = sc.nextInt();
        if (name == null || surname == null || birthdate == null) throw new IllegalArgumentException();
        Human children = new Human(name, surname, birthdate, iq);
        familyController.adoptChild(family, children);
    }

    public static void editFamily(){
        System.out.println("1. Родить ребенка");
        System.out.println("2. Усыновить ребенка");
        System.out.println("3. Вернуться назад");
        int option = sc.nextInt();
        switch (option) {
            case 1:
                bornChild();
                break;
            case 2:
                adoptChild();
                break;
            case 3:
                break;
            default:
                throw new IllegalStateException("Нет такой опции: " + option);
        }
    }
    public static void rmChildrenOlderThen(){
        System.out.println("Введите возраст: ");
        int age = sc.nextInt();
        if (age < 1) throw new IllegalArgumentException();
        familyController.deleteAllChildrenOlderThen(age);
    }

    public static void main(String[] args) {
        boolean stop = false;
        do {
            printMenu();
            System.out.println("Выберите пункт меню");
            String option = sc.nextLine().toLowerCase();
            switch (option) {
                case "1" -> {
                    try {
                        familyController.generateFamilies(7);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                case "2" -> {
                    try {
                        familyController.getData();
                    } catch (IOException | ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                case "3" -> getFamMoreThan();
                case "4" -> getFamLessThan();
                case "5" -> getFamAs();
                case "6" -> createFamily();
                case "7" -> rmFamilyByID();
                case "8" -> editFamily();
                case "9" -> rmChildrenOlderThen();
                case "exit" -> stop = true;
            }
        } while (!stop);
    }
}
