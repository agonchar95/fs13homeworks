public final class Woman extends Human{
    private Family family;

    public Woman(String name, String surname, String birthdate) {
        super(name, surname, birthdate);
    }

    public Woman(String name, String surname, String birthdate, int iq) {
        super(name, surname, birthdate, iq);
    }

    public Woman(String name, String surname, String birthdate, Family family, int iq, Pet pet) {
        super(name, surname, birthdate, family, iq, pet);
    }
    public void greetPet(){
        System.out.printf("Ку-ку мой сладусик %s, ты моя киса!мур-мур \n", this.family.getPets());
    }
    public void setFamily(Family family){
        this.family = family;
    }
    public void hideDefects(){
        System.out.println("Самое время стать красоткой)");
    }
}

