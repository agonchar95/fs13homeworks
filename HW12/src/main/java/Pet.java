import java.util.HashSet;
import java.util.Set;

public abstract class Pet {
    private String nickname;
    private int age;
    private int trickLevel;
    private final Set<String> habits = new HashSet<>();

    public Pet(String nickname) {
        this.nickname = nickname;
    }

    public Pet(String nickname, int age, int trickLevel, String habit) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits.add(habit);
    }

    public abstract Species getSpecies();

    public String getNickname() {
        return this.nickname;
    }

    public int getAge() {
        return this.age;
    }

    public int getTrickLevel() {
        return this.trickLevel;
    }

    public Set<String> getHabits() {
        return this.habits;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public void setHabits(String habit) {
        this.habits.add(habit);
    }

    @Override
    public String toString() {
        return "{" + "nickname = '" +
                this.nickname + '\'' + ", age = " +
                this.age + ", trickLevel = " +
                this.trickLevel + ", habits = " +
                (this.habits)+"}";
    }

    protected void finalize() {
        System.out.printf("%s was deleted right now", this.toString());
    }

    void eat() {
        System.out.println("Я кушаю!");
    }

    void respond() {
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился!\n", this.nickname);
    }

    public String prettyFormat() {
        return String.format("%s: name: '%s', age: '%d', trickLevel: '%d', habits: '%s'\n", getSpecies(), this.nickname, this.age, this.trickLevel, this.habits);
    }


    public abstract void foul();

}
