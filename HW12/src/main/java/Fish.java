public class Fish extends Pet{
    private final Species species = Species.Fish;

    public Fish(String nickname) {
        super(nickname);
    }

    public Fish(String nickname, int age, int trickLevel, String habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.printf("Привет. Я - %s. Я соскучился! \n", this.getNickname());
    }

    public Species getSpecies() {
        return this.species;
    }

    public void foul() {
        System.out.print("Нужно отлично скрыть следы \n");
    }
}

