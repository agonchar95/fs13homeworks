import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class FamilyService {
    public FamilyDao familyDao;

    public FamilyService() throws IOException, ClassNotFoundException {
        familyDao = new CollectionFamilyDao();
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        List<Family> allFamilies = familyDao.getAllFamilies();
        for(int i = 0; i < allFamilies.size(); i++){
            System.out.printf("#%d %s",i+1, allFamilies.get(i).prettyFormat());
        }
    }


    public List<Family> getFamiliesBiggerThan(int number) {
        return familyDao.getAllFamilies().stream()
                .filter(family -> family.familySize() > number)
                .collect(Collectors.toList());
    }

    public List<Family> getFamiliesLessThan(int number) {
        return familyDao.getAllFamilies().stream()
                .filter(family -> family.familySize() < number)
                .collect(Collectors.toList());
    }

    public int countFamiliesWithMemberNumber(int number) {
        return (int) familyDao.getAllFamilies().stream()
                .filter(family -> family.familySize() == number)
                .count();
    }

    public Family createNewFamily(Woman woman, Man man) {
        return familyDao.saveFamily(new Family(woman, man));
    }

    public Family deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }

    public Family bornChild(Family family, String maleName, String femaleName) {
        int rand = (int) (Math.random() * 2);

        Human child = rand == 0 ?
                new Human(maleName, family.familyName(), new SimpleDateFormat("dd/MM/yyyy").format(new Date()))
                :
                new Human(femaleName, family.familyName(), new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
        family.addChild(child);
        return family;
    }

    public Family adoptChild(Family family, Human child) {
        List<Family> all = familyDao.getAllFamilies();
        int index = all.indexOf(family);
        Family selected = all.get(index);
        selected.addChild(child);
        return selected;
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyDao.getAllFamilies().stream()
                .peek(family -> family.getChildren()
                        .removeIf(human -> new Date().getYear() - new Date(human.getYear()).getYear() > age))
                .close();
    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return familyDao.getFamilyByIndex(index);
    }

    public String getPets(int index) {
        Family family = familyDao.getFamilyByIndex(index);
        return family.getPets();
    }

    public boolean addPet(int index, Pet pet) {
        Family family = familyDao.getFamilyByIndex(index);
        family.setPets(pet);
        return true;
    }

    public void generateFamilies(int count) throws IOException {
        String name = "TestName";
        String surname = "TestSurname";
        for (int i = 0; i < count; i++) {
            Man father = new Man(name + i+1, surname + i+1, "11/07/1993");
            Woman mother = new Woman(name + i+1, surname + i+1, "15/11/1995");
            Human child = new Human(name + i+1, surname + i+1, "10/07/2020");
            familyDao.saveFamily(new Family(mother, father, child));
        }
        List<Family> families = getAllFamilies();
        saveData(families);
    }

    public void saveData(List<Family> data) throws IOException {
        familyDao.saveData(data);
    }
    public List<Family> getData() throws IOException, ClassNotFoundException {
        return familyDao.getData();
    }

}
