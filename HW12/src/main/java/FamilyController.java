import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FamilyController {
    public FamilyService familyService;

    public FamilyController() throws IOException, ClassNotFoundException {
        familyService = new FamilyService();
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public void generateFamilies (int count) throws IOException {
        familyService.generateFamilies(count);
    }

    public List<Family> getFamiliesBiggerThan(int number) {
        return familyService.getFamiliesBiggerThan(number);
    }

    public List<Family> getFamiliesLessThan(int number) {
        return familyService.getFamiliesLessThan(number);
    }

    public int countFamiliesWithMemberNumber(int number) {
        return familyService.countFamiliesWithMemberNumber(number);
    }

    public Family createNewFamily(Woman woman, Man man) {
        return familyService.createNewFamily(woman, man);
    }

    public Family deleteFamilyByIndex(int index) {
        return familyService.deleteFamilyByIndex(index);
    }

    public Family bornChild(Family family, String maleName, String femaleName) {
        return familyService.bornChild(family, maleName, femaleName);
    }

    public Family adoptChild(Family family, Human child) {
        return familyService.adoptChild(family, child);
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyService.deleteAllChildrenOlderThen(age);
    }

    public void count() {
        System.out.println("total Families:" + familyService.count()); ;
    }

    public Family getFamilyById(int index) {
        return familyService.getFamilyById(index);
    }

    public String getPets(int index) {
        return familyService.getPets(index);
    }

    public boolean addPet(int index, Pet pet) {
        return familyService.addPet(index, pet);
    }

    public void saveData(List<Family> data) throws IOException { familyService.saveData(data);}
    public List<Family> getData() throws IOException, ClassNotFoundException {return familyService.getData();}

}
