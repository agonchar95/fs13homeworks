import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CollectionFamilyDao implements FamilyDao, Serializable {
    final List<Family> familyData = new ArrayList<>();

    public CollectionFamilyDao() throws IOException, ClassNotFoundException {
    }


    @Override
    public List<Family> getAllFamilies() {
        return this.familyData;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return this.familyData.get(index);
    }

    @Override
    public Family deleteFamily(int index) {
        return this.familyData.remove(index);
    }

    @Override
    public Boolean deleteFamily(Family family) {
        return this.familyData.remove(family);
    }

    @Override
    public Family saveFamily(Family family) {
        this.familyData.add(family);
        return family;
    }

    @Override
    public void saveData(List<Family> data) throws IOException {
        File file = new File("DB.bin");
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(data);
        oos.close();

    }

    @Override
    public List<Family> getData() throws IOException, ClassNotFoundException {
            File file = new File("DB.bin");
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            List<Family> families = (List<Family>) ois.readObject();
            System.out.println("Data from DB:");
            System.out.println(families.stream().map(Family::prettyFormat).collect(Collectors.joining()));
            ois.close();
            return families;
    }
}
