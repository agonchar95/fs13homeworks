import java.util.Arrays;
import java.util.Scanner;

public class JavaApp {

    public static int random(int min, int max) {
        return min + (int) (Math.random()*((max - min)));
    }

    public static void buildField(char[][] array, int num) {
        for (int x = 0; x <= num; x++) {
            System.out.print(x + " | ");
        }
        System.out.println();
        for (int i = 0; i < num; i++) {
            System.out.print(i + 1 + " |");
            for (int j = 0; j < num; j++) {
                System.out.printf(" %c |",array[i][j]);
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("All set. Get ready to rumble!");
        int size = 5;
        char[][] area = new char[size][size];
        int randomX = random(0, 5);
        int randomY = random(0, 5);
        System.out.println(randomX);
        System.out.println(randomY);
        boolean stop = false;
        for (char[] chars : area) {
            Arrays.fill(chars, '-');
        }
        do {
            buildField(area, size);
            int x = 0;
            int y = 0;

            while (!(x > 0 && x <= size)) {
                System.out.println("Enter line to shoot: ");
                while (!scanner.hasNextInt()) {
                    scanner.next();
                    System.out.println("Enter number from 0 to 5!");
                }
                x = scanner.nextInt();
            }
            x--;
            while (!(y > 0 && y <= size)) {
                System.out.println("Enter column to shoot: ");
                while (!scanner.hasNextInt()) {
                    System.out.println("Enter number from 0 to 5!");
                    scanner.next();
                }
                y = scanner.nextInt();
            }
            y--;
            if (x == randomX && y == randomY) {
                area[x][y] = 'X';
                buildField(area, size);
                stop = true;
            } else {
                area[x][y] = '*';
            }
        } while (!stop);
        System.out.println("YOU WON!!!!!");
    }
}
