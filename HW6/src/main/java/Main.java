public class Main {
    public static void main(String[] args) {

        Human Vasya = new Man("Vasya", "Ivanov", 1995);
        Human Masha = new Woman("Masha", "Ivanova", 1997);
        Human Dasha = new Woman("Dasha", "Ivanova", 2020);
        Family family1 = new Family(Vasya, Masha);
        family1.addChild(Dasha);
        System.out.println(family1);
        System.out.println("---------------------------------------------------");

        Fish fish = new Fish("Screamer");
        System.out.println(fish);
        fish.foul();
    }
}
