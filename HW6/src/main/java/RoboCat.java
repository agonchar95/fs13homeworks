public class RoboCat extends Pet{
    private final Species species = Species.UNKNOWN;

    public RoboCat(String nickname) {
        super(nickname);
    }

    public RoboCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.printf("Привет хозяин. Я - %s. Я соскучился! \n", this.getNickname());
    }
    public Species getSpecies(){
        return this.species;
    }

    @Override
    public void foul() {
        System.out.print("Мне не нужно уничтожать следы)\n");
    }
}

