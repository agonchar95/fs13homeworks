import java.util.Arrays;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet;
    private Family family;
    private String[][] schedule = new String[1][2];

    public Human (String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human (String name, String surname, int year, Family family) {
        this(name, surname, year);
        this.family = family;
    }

    public Human (String name, String surname, int year, Family family, int iq, Pet pet, String[][] schedule) {
        this(name, surname, year, family);
        this.iq = iq;
        this.pet = pet;
        this.schedule = schedule;
    }

    public Human() {

    }

    public String getName() {
        return this.name;
    }

    public String getSurname() {
        return this.surname;
    }

    public int getYear() {
        return this.year;
    }

    public int getIq() {
        return this.iq;
    }

    public Pet getPet() {
        return this.pet;
    }

    public String[][] getSchedule() {
        return this.schedule;
    }

    public Family getFamily () {
        return this.family;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void setSchedule(DayOfWeek day, String task) {
        String[] dailyTask = new String[]{day.name(), task};
        String[][] personSchedule = this.getSchedule();
        String[][] newSchedule;
        for (int i = 0; i < personSchedule.length; i++) {
            for (int j = 0; j < personSchedule[i].length; j++) {
                if(personSchedule[i][j] == null){
                    personSchedule[i] = dailyTask;
                    this.schedule = personSchedule;
                    return;
                }
                else if(personSchedule[i][j] !=null){
                    newSchedule = Arrays.copyOf(personSchedule, personSchedule.length + 1);
                    newSchedule[newSchedule.length - 1] = dailyTask;
                    this.schedule = newSchedule;
                    return;
                }
            } break;
        }

    }

    public void setFamily (Family family) {
        this.family = family;
    }

    @Override
    public String toString() {
        return this.getClass() + "{ " +
                "name='" + this.name + '\'' + ", surname='" +
                this.surname + '\'' + ",year = " + this.year + ",family = " + this.family +
                ", iq = " + this.iq + ",schedule = "
                + Arrays.deepToString(this.schedule) + ", " + this.pet + "}";
    }

    protected void finalize() {
        System.out.printf("%s was deleted right now", this.toString());
    }
    
    void greetPet() {
        System.out.printf("Привет, %s\n", pet.getNickname());
    }

    void describePet() {
        String trick = (pet.getTrickLevel() <= 50 ? "почти не хитрый" : "очень хитрый");
        System.out.printf("У меня есть %s, eмy %d лет, он %s\n", pet.getSpecies(), pet.getAge(), trick);
    }
}
