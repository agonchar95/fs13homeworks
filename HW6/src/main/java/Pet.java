import java.util.Arrays;

public abstract class Pet {
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    public Pet(String nickname) {
        this.nickname = nickname;
    }

    public Pet(String nickname, int age, int trickLevel, String[] habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public abstract Species getSpecies();

    public String getNickname() {
        return this.nickname;
    }

    public int getAge() {
        return this.age;
    }

    public int getTrickLevel() {
        return this.trickLevel;
    }

    public String getHabits() {
        return Arrays.toString(this.habits);
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    @Override
    public String toString() {
        return "{" + "nickname = '" +
                this.nickname + '\'' + ", age = " +
                this.age + ", trickLevel = " +
                this.trickLevel + ", habits = " +
                Arrays.toString(this.habits) + "}";
    }

    protected void finalize() {
        System.out.printf("%s was deleted right now", this.toString());
    }

    void eat() {
        System.out.println("Я кушаю!");
    }

    void respond() {
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился!\n", this.nickname);
    }

    public abstract void foul();

}
