public class Main {
    public static void main(String[] args) {
        String[][] schedule = {
                {"Monday", "Work"},
                {"Tuesday", "Pray"},
                {"Wednesday", "Lay"},
                {"Thursday", "Fly"},
                {"Friday", "Cry"},
                {"Saturday", "Chill"},
                {"Sunday", "Dream"}};

        Human Vasya = new Human("Vasya", "Ivanov", 1995);
        Human Masha = new Human("Masha", "Ivanova", 1997);
        Human Dasha = new Human("Dasha", "Ivanova", 2020);
        Dasha.setSchedule(schedule);

        Family family1 = new Family(Vasya, Masha);
        family1.addChild(Dasha);
        System.out.println(family1);
        System.out.println("---------------------------------------------------");

        Pet Chih = new Pet("dog", "Chih");
        System.out.println(Chih);
        Dasha.setPet(Chih);
        System.out.println(Dasha);
        Dasha.greetPet();
        Dasha.describePet();
        System.out.println("---------------------------------------------------");
    }
}
