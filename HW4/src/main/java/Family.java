import java.util.Arrays;

public class Family {
    private Human mother;
    private Human father;
    private Human [] children;
    private Pet pet;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.children = new Human[0];
    }

    public Human getMother() {
        return this.mother;
    }

    public Human getFather() {
        return this.father;
    }

    public Human [] getChildren() {
        return this.children;
    }

    public Pet getPet() {
        return this.pet;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        return this.mother.toString() + "\n" +
                this.father.toString() + "\n" + "children " +
                Arrays.toString(this.children) + "\n" +
                (this.pet == null ? "" : pet.toString());
    }

    public Human[] addChild(Human child) {
        Human[] newChildren = new Human[children.length+1];
        for (int i = 0; i < children.length; i++) {
            newChildren[i] = children[i];
        }
        newChildren[children.length] = child;
        this.children = newChildren;
        return children;
    }

    public boolean deleteChild(int index) {
        if (children == null || index < 0 || index > children.length) {
            return false;
        } else {
            Human[] newChildren = new Human[children.length - 1];
            for (int i = index; i < children.length - 1; i++) {
                newChildren[i] = children[i + 1];
            }
            this.children = newChildren;
        }
        return true;
    }

}
